from rest_framework import serializers

from questionnaire.quizzes.api.serializers import QuizProgressSerializer
from questionnaire.users.models import User


class UserSerializer(serializers.ModelSerializer):
    quiz_progresses = QuizProgressSerializer(many=True)

    class Meta:
        model = User
        fields = ["id", "username", "name", "quiz_progresses"]
