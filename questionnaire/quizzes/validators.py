import jsonschema
from django.core import exceptions
from django.utils.translation import gettext_lazy as _

SINGLE_CHOICE_SCHEMA = {
    "type": "object",
    "additionalProperties": False,
    "properties": {
        "answer": {"type": "number"},
        "choices": {
            "type": "array",
            "items": {"type": "string"},
            "minItems": 2,
            "maxItems": 10,
            "uniqueItems": True,
        },
        "description": {"type": "string"},
        "question": {"type": "string"},
    },
    "required": [
        "answer",
        "choices",
        "description",
        "question",
    ],
}

ANSWERS_SCHEMA = {
    "type": "object",
    "additionalProperties": False,
    "properties": {
        "answers": {
            "type": "array",
            "items": {
                "type": "object",
                "additionalProperties": False,
                "properties": {
                    "question_id": {"type": "number"},
                    "answer": {"type": "array"},
                },
                "required": [
                    "question_id",
                    "answer",
                ],
            },
        },
        "quiz_progress_id": {"type": "number"},
    },
    "required": [
        "answers",
        "quiz_progress_id",
    ],
}


def validate_single_choice(data):
    try:
        jsonschema.validate(instance=data, schema=SINGLE_CHOICE_SCHEMA)
        if not (0 <= data["answer"] < len(data["choices"])):
            raise jsonschema.exceptions.ValidationError(
                "The 'answer' is a number that corresponds to the specific index in the field 'choices'!"
            )
    except jsonschema.exceptions.ValidationError as error:
        # TODO: possibly not the write way of debugging
        print()
        print("Inputted data failed against JSON schema check.")
        print(f"The data:\n{data}")
        print(f"The error:\n{error}")
        print()
        raise exceptions.ValidationError(
            _(
                "Inputted data failed against JSON schema validation for single-choice questions!"
            )
        )


def validate_quiz_check(request_data):
    try:
        jsonschema.validate(instance=request_data, schema=ANSWERS_SCHEMA)
    except jsonschema.exceptions.ValidationError as error:
        print()
        print("Inputted data failed against JSON schema check.")
        print(f"The data:\n{request_data}")
        print(f"The error:\n{error}")
        print()
        raise exceptions.ValidationError(
            _(
                "Request data failed against JSON schema validation in checking the quiz!"
            )
        )
