from django.contrib import admin
from django.contrib.postgres import fields
from django_json_widget.widgets import JSONEditorWidget

from .models import Question, Quiz, QuizProgress


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    list_display = ["id", "type", "difficulty"]
    formfield_overrides = {
        fields.JSONField: {"widget": JSONEditorWidget},
    }


@admin.register(Quiz)
class QuizAdmin(admin.ModelAdmin):
    list_display = ["id", "title", "description"]


@admin.register(QuizProgress)
class QuizProgressAdmin(admin.ModelAdmin):
    list_display = ["id", "user", "quiz", "number_of_questions"]
    readonly_fields = ["number_of_questions", "number_of_correctly_answered", "decimal"]
