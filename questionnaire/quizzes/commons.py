SINGLE_CHOICE_CORRECT_DATA = {
    "answer": 1,
    "choices": ["1", "4", "5", "7"],
    "question": "How many oceans are there in the world?",
    "description": "Answer the question.",
}

FILL_IN_THE_BLANKS_CORRECT_DATA = {
    "answers": ["fruit", "vegetable"],
    "question": "Apple is a %blank%. Potato is a %blank%.",
    "description": "Fill in the blanks with appropriate words.",
    "number_of_blanks": 2,
}

MULTIPLE_CHOICE_CORRECT_DATA = {
    "answers": [1, 2],
    "choices": ["Book", "Read", "Jump", "Literature"],
    "question": "Which words are actions here?",
    "description": "Find all possible answers.",
}
