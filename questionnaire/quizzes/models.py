from django.contrib.postgres import fields
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import gettext_lazy as _

from questionnaire.users.models import User

from .validators import validate_single_choice


class Question(models.Model):
    class Meta:
        verbose_name = _("Question")
        verbose_name_plural = _("Questions")

    class Type(models.IntegerChoices):
        SINGLE_CHOICE = 1, _("Single-choice")
        MULTIPLE_CHOICE = 2, _("Multiple-choice")
        FILL_IN_THE_BLANKS = 3, _("Fill-in-the-blanks")

    class Difficulty(models.IntegerChoices):
        EASY = 1, _("Easy")
        MEDIUM = 2, _("Medium")
        HARD = 3, _("Hard")

    type = models.PositiveSmallIntegerField(_("Type"), choices=Type.choices, null=False)
    data = fields.JSONField(
        _("Data"),
        help_text=_("The structure is important and is validated against JSON schema."),
    )
    difficulty = models.PositiveSmallIntegerField(
        _("Difficulty"), choices=Difficulty.choices, null=False
    )

    def clean(self) -> None:
        # TODO: add validations for other types of questions
        if self.type == self.Type.SINGLE_CHOICE:
            validate_single_choice(self.data)

    # Override save function to call self.full_clean() which in turn calls self.clean()
    def save(self, *args, **kwargs):
        self.full_clean()
        return super().save(*args, **kwargs)  # Call the "real" save() method

    def check_answer(self, answer):
        if self.type == self.Type.SINGLE_CHOICE:
            if not (len(answer) == 1 and isinstance(answer[0], int)):
                raise ValidationError(
                    "When the question type is single-choice, the 'answer' is an array of a single element that is "
                    "integer! "
                )
            return self.data["answer"] == answer[0]
        elif (
            self.type == self.Type.MULTIPLE_CHOICE
            or self.type == self.Type.FILL_IN_THE_BLANKS
        ):
            return self.data["answers"] == answer
        else:
            raise ValidationError(
                f"Server problem: the question of type {self.type} is not coded yet in "
                f"Question.check_answer()!"
            )


class Quiz(models.Model):
    class Meta:
        verbose_name = _("Quiz")
        verbose_name_plural = _("Quizzes")

    title = models.CharField(_("Title"), max_length=50)
    description = models.CharField(_("Description"), max_length=200, blank=True)
    questions = models.ManyToManyField(Question, verbose_name=_("Questions"))


class QuizProgress(models.Model):
    class Meta:
        verbose_name = _("Quiz Progress")
        verbose_name_plural = _("Quiz Progresses")
        unique_together = ["user", "quiz"]

    user = models.ForeignKey(
        User,
        related_name="quiz_progresses",
        on_delete=models.CASCADE,
        verbose_name=_("User"),
    )
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE, verbose_name=_("Quiz"))
    number_of_questions = models.PositiveIntegerField(_("Number of questions"))
    number_of_correctly_answered = models.PositiveIntegerField(
        _("Number of correctly answered"), default=0
    )
    decimal = models.DecimalField(
        _("Points in decimal"), max_digits=5, decimal_places=4, default=0.0000
    )

    def save(self, *args, **kwargs):
        self.number_of_questions = len(self.quiz.questions.all())
        self.decimal = self.number_of_correctly_answered / self.number_of_questions
        return super().save(*args, **kwargs)  # Call the "real" save() method

    def check_quiz(self, request_data):
        questions = self.quiz.questions.all()
        answers = request_data["answers"]

        if len(questions) != len(answers):
            raise ValidationError(
                f"The array 'user_answers' consists of {len(questions)} questions, not {len(answers)}!"
            )

        all_unique = {}
        self.number_of_correctly_answered = 0
        for answer in answers:
            question_id = answer["question_id"]
            if question_id in all_unique:
                raise ValidationError(
                    f"Given 'question_id'-s must be unique: {question_id} is repeated!"
                )
            all_unique[question_id] = True

            if not questions.filter(id=question_id).exists():
                raise ValidationError(
                    f"There is no question with id {question_id} in the current quiz!"
                )

            question = questions.get(id=question_id)

            if question.check_answer(answer["answer"]):
                self.number_of_correctly_answered += 1

        self.save()
