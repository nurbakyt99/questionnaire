import pytest
from django.core.exceptions import ValidationError

from ..models import Question

pytestmark = pytest.mark.django_db

CORRECT_DATA = {
    "answer": 1,
    "choices": ["1", "4", "5", "7"],
    "question": "How many oceans are there in the world?",
    "description": "Answer the question.",
}

WRONG_DATA_1 = {
    "choices": ["1", "4", "5", "7"],
    "question": "How many oceans are there in the world?",
    "description": "Answer the question.",
}

WRONG_DATA_2 = {
    "answer": 4,
    "choices": ["1", "4", "5", "7"],
    "question": "How many oceans are there in the world?",
    "description": "Answer the question.",
}

WRONG_DATA_3 = {
    "answer": 0,
    "choices": ["1"],
    "question": "How many oceans are there in the world?",
    "description": "Answer the question.",
}


def test_question_create_without_type():
    question = Question(data=CORRECT_DATA, difficulty=Question.Difficulty.EASY)
    with pytest.raises(ValidationError):
        question.save()


def test_question_create_without_difficulty():
    question = Question(
        type=Question.Type.SINGLE_CHOICE,
        data=CORRECT_DATA,
    )
    with pytest.raises(ValidationError):
        question.save()


def test_question_create():
    question = Question(
        type=Question.Type.SINGLE_CHOICE,
        data=CORRECT_DATA,
        difficulty=Question.Difficulty.EASY,
    )
    question.save()
    assert Question.objects.count() == 1


def test_question_create_with_wrong_data():
    question = Question(
        type=Question.Type.SINGLE_CHOICE,
        data=WRONG_DATA_1,
        difficulty=Question.Difficulty.EASY,
    )
    with pytest.raises(ValidationError):
        question.save()

    question.data = WRONG_DATA_2
    with pytest.raises(ValidationError):
        question.save()

    question.data = WRONG_DATA_3
    with pytest.raises(ValidationError):
        question.save()
