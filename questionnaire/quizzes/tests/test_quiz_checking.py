import pytest

from questionnaire.users.models import User

BASE_URL = "http://localhost:8000/"
API_URL = BASE_URL + "api/v0/"


@pytest.fixture
def api_client():
    from rest_framework.test import APIClient

    return APIClient()


@pytest.mark.django_db
def test_unauthorized_request(api_client):
    url = API_URL + "quizzes/"
    response = api_client.get(url)
    print(response, response.content)
    assert response.status_code == 401


def test_should_create_user_with_username(db):
    user = User.objects.create_user("Haki")
    assert user.username == "Haki"


@pytest.fixture
def user(db):
    return User.objects.create_user(
        username="pytest", password="Pytest-django-pass@", email="pytest@pytest.com"
    )


# @pytest.fixture
# def test_token(user, api_client):
#     url = BASE_URL + 'api-token-auth/'
#     response = api_client.post(url, format='json', data={
#         "username": 'ivan',
#         "password": 'ivan1234',
#     })
#     print(response, response.content, response.request)
#     assert response.status_code == 200


# @pytest.fixture
# def test_register(db, user, api_client):
#     url = BASE_URL + 'rest-auth/registration/'
#     response = api_client.post(url, data={
#         "username": user.username,
#         # "email": ,
#         "password1": user.password,
#         "password2": user.password
#     })
#     print(response, response.content)
#     assert response.status_code == 200


# @pytest.fixture
# def test_login(db, user, api_client):
#     url = BASE_URL + 'rest-auth/login/'
#     response = api_client.post(url, data=
#         {
#             "username": user.username,
#             "password": user.password
#         })
#     print(response, response.content)
#     assert response.status_code == 200
#
