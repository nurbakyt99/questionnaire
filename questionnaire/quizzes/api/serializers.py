from rest_framework import serializers

from ..models import Question, Quiz, QuizProgress


class QuestionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Question
        fields = ["url", "type", "data"]


class QuizProgressSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = QuizProgress
        fields = [
            "url",
            "user",
            "quiz",
            "number_of_questions",
            "number_of_correctly_answered",
            "decimal",
        ]


class QuizSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Quiz
        fields = ["url", "title", "description"]


class QuizDetailSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Quiz
        fields = ["url", "title", "description", "questions"]
