from rest_framework import routers

from .views import QuestionViewSet, QuizProgressViewSet, QuizViewSet

router = routers.DefaultRouter()
router.register(r"questions", QuestionViewSet)
router.register(r"progresses", QuizProgressViewSet)
router.register(r"", QuizViewSet)

urlpatterns = router.urls
