from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from rest_framework import permissions, status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from ..models import Question, Quiz, QuizProgress
from ..validators import validate_quiz_check
from .serializers import (
    QuestionSerializer,
    QuizDetailSerializer,
    QuizProgressSerializer,
    QuizSerializer,
)


class QuestionViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows questions to be viewed.
    """

    queryset = Question.objects.all()
    serializer_class = QuestionSerializer
    permission_classes = [permissions.IsAuthenticated]


class QuizProgressViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows Quiz Progresses to be viewed.
    """

    queryset = QuizProgress.objects.all()
    serializer_class = QuizProgressSerializer
    permission_classes = [permissions.IsAuthenticated]


class QuizViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows quizzes to be viewed.
    """

    queryset = Quiz.objects.all()
    serializer_class = QuizSerializer
    permission_classes = [permissions.IsAuthenticated]

    @action(detail=True, methods=["post"])
    def start(self, request, pk):
        quiz = self.get_object()

        quiz_progress, created = QuizProgress.objects.get_or_create(
            user=request.user, quiz=quiz
        )

        data = {
            "status": _("Success!"),
            "quiz": QuizDetailSerializer(quiz, context={"request": request}).data,
            "quiz_progress": QuizProgressSerializer(
                quiz_progress, context={"request": request}
            ).data,
        }
        return Response(data, status=status.HTTP_201_CREATED)

    @action(detail=True, methods=["post"])
    def check(self, request, pk):
        try:
            validate_quiz_check(request.data)
            quiz_progress = QuizProgress.objects.get(
                id=request.data["quiz_progress_id"]
            )
            if quiz_progress.user != request.user:
                raise ValidationError(
                    "You do not have access to the given quiz progress!"
                )
            if int(quiz_progress.quiz.id) != int(pk):
                raise ValidationError(
                    "This quiz doesn't belong to the given quiz progress!"
                )

            quiz_progress.check_quiz(request.data)

        except QuizProgress.DoesNotExist:
            return Response(
                {"error": "Given 'quiz_progress_id' does not exist!"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        except ValidationError as error:
            return Response({"error": error}, status=status.HTTP_400_BAD_REQUEST)

        else:
            return Response(
                {
                    "status": "Success!",
                    "quiz_progress": QuizProgressSerializer(
                        quiz_progress, context={"request": request}
                    ).data,
                }
            )
